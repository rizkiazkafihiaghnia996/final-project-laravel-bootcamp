<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'category_name' => "required"
        ]);

        $categories = new Categories;
        $categories->category_name = $request['category_name'];

        $categories->save();

        return redirect('/categories')->with('success', 'Category was created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function index()
    {
        $categoriess = Categories::all();
        return view('pages.categories.index', compact('categoriess'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function show($id)
    {
        $categories = Categories::find($id);
        return view('pages.categories.show', compact('categories'));
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categories::find($id);
        return view('pages.categories.edit', compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request -> validate([
            'category_name' => 'required',
        ]);
        
        $update = Categories::where('id', $id)->update([
            'category_name'=>$request['category_name'],
        ]);

        return redirect('/categories')->with('success', 'Changes saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Categories::destroy($id);
        return redirect('/categories')->with('success', 'Category deleted!');
    }
}
