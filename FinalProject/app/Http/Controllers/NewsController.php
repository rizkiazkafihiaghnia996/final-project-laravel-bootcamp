<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\News;
use App\Categories;
use App\User;
use Auth;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create()
    {
        $categoriesss = Categories::all();
        return view('pages.news.create', compact('categoriesss'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request -> validate([
            'title' => 'required',
            'body' => 'required',
            'categories_id' => 'required'
        ]);

        $image =  $request->poster;
        $name_img = time(). " - ". $image->getClientOriginalName();

        $news = new News;
        $news->title = $request['title'];
        $news->sub_title = $request['sub_title'];
        $news->body = $request['body'];
        $news->poster = $name_img;
        $news->users_id = Auth::id();
        $news->categories_id = Auth::id();
        
        $news->save();
        $image->move('img', $name_img);

        return redirect('/news')->with('success', 'News was posted successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $newss = News::all();
        return view('pages.news.index', compact('newss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $news = News::find($id);
        $categoriessss = Categories::find($id);
        $users = User::find($id);
        return view('pages.news.show')->with(compact('news', $news))->with(compact('categoriessss', $categoriessss))->with(compact('users', $users));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        $categoriesss = Categories::all();
        $users = User::find($id);
        return view('pages.news.edit')->with(compact('news', $news))->with(compact('categoriesss', $categoriesss))->with(compact('users', $users));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate([
            'title' => 'required',
            'body' => 'required',
            'categories_id' => 'required'
        ]);

        $news = News::findorfail($id);
        if ($request->has('poster')) {
            $path="img/";
            File::delete($path. $news->poster);
            $image =  $request->poster;
            $new_img = time(). " - ". $image->getClientOriginalName();
            $image->move('img', $new_img);
            $update = News::where('id', $id)->update([
                'title' => $request['title'],
                'sub_title' => $request['sub_title'],
                'body' => $request['body'],
                'poster' => $new_img,
                'categories_id' => Auth::id()
            ]);
        }
        else {
            $update = News::where('id', $id)->update([
                'title' => $request['title'],
                'sub_title' => $request['sub_title'],
                'body' => $request['body'],
                'categories_id' => Auth::id()
            ]);
        }

        return redirect('/news')->with('success', 'Changes saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = News::findorfail($id);
        $destroy->delete();
        $path = "img/";
        File::delete($path. $destroy->poster);
        return redirect('/news')->with('success', 'News was successfully deleted!');
    }
}
