<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item pt-2">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block pt-2">
        <a class="nav-link" href="{{route('news.create')}}">Write a News</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block pt-2">
        <a class="nav-link" href="{{route('categories.create')}}">Create a Category</a>
      </li>
    </ul>

    <!-- Right navbar links -->
  </nav>