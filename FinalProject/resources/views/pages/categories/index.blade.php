@extends('layouts.master')

@section('content')
    <div class='ml-3 mt-3 mr-3'>
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Category</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                  @endif
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th style="width: 500px">Categories</th>
                      <th style="width: 110px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($categoriess as $key => $category)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>
                                <a href="/categories/{{$category->id}}">
                                    {{ $category -> category_name }}
                                </a>
                            </td>
                            <td style="display: flex">
                                <a href="/categories/{{$category->id}}/edit" class="btn btn-success btn-sm mr-1">Edit</a>
                                <form action="/categories/{{$category->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value='Delete' class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center">No Category Found</td>
                            </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body 
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
              -->
            </div>
    </div>
@endsection