@extends('layouts.master')

@section('content')
        <div class="ml-3 mt-3 mr-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create a Category</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/categories" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nameInput">Category Name</label>
                    <input type="text" class="form-control" name="category_name" value="{{ old('category_name', '') }}" id="nameInput" placeholder="Category name goes here...">
                    @error('category_name')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
@endsection