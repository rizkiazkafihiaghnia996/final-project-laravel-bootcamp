@extends('layouts.master')

@section('content')
        <div class="ml-3 mt-3 mr-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Write a News</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/news" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="titleInput">Title</label>
                    <input type="text" class="form-control" name="title" value="{{ old('title', '') }}" id="titleInput" placeholder="Title of the news goes here...">
                    @error('title')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="sub_titleInput">Sub Title</label>
                    <input type="text" class="form-control" name="sub_title" value="{{ old('sub_title', '') }}" id="sub_titleInput" placeholder="Sub Title of the news goes here...">
                    @error('sub_title')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bodyInput">Body</label>
                    <input type="text" class="form-control" name="body" id="bodyInput" value="{{ old('body', '') }}" placeholder="News goes here...">
                    @error('body')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="categoriesInput">Category</label><br>
                    <select name="categories_id" id="categoriesInput" class="form-control">
                      <option value="">-</option>
                      @foreach ($categoriesss as $value) 
                        <option value="{{$value->id}}">{{$value->category_name}}</option>
                      @endforeach
                    </select>
                    @error('categories_id')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="posterInput">Poster</label>
                    <input type="file" class="form-control" name="poster" value="{{ old('poster', '') }}" id="posterInput" placeholder="Upload a Poster">
                    @error('poster')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
@endsection