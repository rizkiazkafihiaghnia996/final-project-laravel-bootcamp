@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card">
        <div class="ml-2 mr-2">
            <h1>{{$news->title}}</h1>
            <h5>{{$news->sub_title}}</h5>
            <h7>Posted on: {{$news->created_at}}</h7>
            <img src="{{asset('img/'.$news->poster)}}" height="500px" class="card-img-top" alt="...">
            <p>{{$news->body}}</p>
        </div>
    </div>
</div>
@endsection