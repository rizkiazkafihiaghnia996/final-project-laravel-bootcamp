@extends('layouts.master')

@section('content')
    @forelse($newss as $value)
        <div class="card" style="width: 90rem">
            <div class="card-body">
                <div class="card" style="width: 88rem">
                    <img src="{{asset('img/'.$value->poster)}}"  height="500px" class="card-img-top" alt="...">
                    <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                        <h1 class="card-title">
                            <strong><a href="/news/{{$value->id}}">
                                {{$value->title}}
                            </a></strong>
                        </h1><br>
                        <p class="card-text">{{$value->created_at}}</p>
                        <form action="{{route('news.destroy', ['news' => $value->id])}}" method="POST">
                            <a href="/news/{{$value->id}}/edit" class="btn btn-success">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" value='Delete' class="btn btn-danger">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <h1 align="center">No News Available</h1>
    @endforelse
@endsection